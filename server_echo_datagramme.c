#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

/**
 * Serveur echo UDP
 * pour tester utiliser
 * nc -vu 127.0.0.1 PORT
 */


// Taille du buffer de réception des trames.
#define LEN_BUFFER 16384

struct addrinfo* addrServ;
int socketEcouteServeur;

/**
 * Procédure d'interception de signaux pour une fermeture propre
 * @param sig
 */
void interceptFinServeur(int sig) {

	printf("\rTerminaison du serveur\n");
	close(socketEcouteServeur);
	exit(EXIT_SUCCESS);
}

/**
 * Programme principal en UDP (Mode datagramme)
 */
int main(int argsNbr, char** args) {

	// On vérifie le nombre d'arguments et affiche une aide s'il en manque
	if (argsNbr < 2) {
		char* posSlash = strrchr(args[0], '/');
		printf("Usage: %s [port number]\n", posSlash == NULL ? args[0] : posSlash + 1);
		return EXIT_SUCCESS;
	}

	// 1 - 1023 Ports réservés
	// 1024 - 49151 Ports standards enregistrés
	// 49152 - 65535 Ports indéfinis
	long portNumber = strtol(args[1], NULL, 10);

	// On vérifie que le numéro de port est cohérent
	if (portNumber <= 0 || portNumber >= 65536) {
		printf("Le numéro de port doit être compris entre 1 et 65535\n");
		return EXIT_SUCCESS;
	} else if (portNumber < 1024 && getuid() != 0) {
		printf("Les numéros de ports inférieurs à 1024 sont réservés au super administrateur\n");
		return EXIT_SUCCESS;
	}

	// On passe à l'ouverture du socket
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET6; // V6 puisque ça va gérer le V4 ET v6 (encore une doc mal faite)
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE;

	// On récupère les paramètres manquants
	int retour;
	if ((retour = getaddrinfo(NULL, args[1], &hints, &addrServ)) != 0) {
		printf("Error getaddrinfo: %s\n", gai_strerror(retour));
		freeaddrinfo(addrServ);
		return EXIT_FAILURE;
	}

	// On ouvre le socket
	if ((socketEcouteServeur = socket(addrServ->ai_family, addrServ->ai_socktype, addrServ->ai_protocol)) == -1) {
		printf("Erreur à l'ouverture du socket\n");
		freeaddrinfo(addrServ);
		return EXIT_FAILURE;
	}

	// On rattache le socket au port
	if (bind(socketEcouteServeur, addrServ->ai_addr, addrServ->ai_addrlen) != 0) {
		close(socketEcouteServeur);
		freeaddrinfo(addrServ);
		printf("Erreur de bind\n");
		return EXIT_FAILURE;
	}

	freeaddrinfo(addrServ);

	// On intercepte les signaux pour une fermeture propre
	signal(SIGINT, interceptFinServeur);
	signal(SIGKILL, interceptFinServeur);
	signal(SIGTERM, interceptFinServeur);

	// On prépare les infos pour les connexions clients
	char buffer[LEN_BUFFER];
	struct sockaddr_in6 addr_client;
	socklen_t len_addr_client = sizeof addr_client;
	ssize_t nbrBytesLus;

	while (1) {

		// Lecture du socket
		if ((nbrBytesLus = recvfrom(socketEcouteServeur, buffer, LEN_BUFFER, 0, (struct sockaddr *) &addr_client, &len_addr_client)) == -1) {
			printf("Erreur de lecture\n");
			return EXIT_FAILURE;
		}

		// On affiche les détails du client
		char addrstr[INET6_ADDRSTRLEN];
		inet_ntop(addr_client.sin6_family, &addr_client.sin6_addr, addrstr, sizeof(addrstr));
		if (strncmp(addrstr, "::ffff:", 7) == 0) printf("Client connecté depuis %s Port: %d\n", &addrstr[7], ntohs(addr_client.sin6_port));
		else printf("Client connecté depuis %s Port: %d\n", addrstr, ntohs(addr_client.sin6_port));

		// On renvoie la chaine au client
		if (sendto(socketEcouteServeur, buffer, nbrBytesLus, 0, (struct sockaddr*) &addr_client, len_addr_client) == -1) {
			printf("Erreur d'écriture\n");
			return EXIT_FAILURE;
		}
	}
}
