#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>

/**
 * Serveur echo TCP
 * Utilise le multithreading pour gérer plusieurs clients en parallèle
 * pour tester utiliser
 * telnet 127.0.0.1 PORT
 */


// Taille du buffer de réception des trames.
#define LEN_BUFFER 16384

struct addrinfo* addrServ;
int socketEcouteServeur;

/**
 * Procédure d'interception de signaux pour une fermeture propre
 * @param sig
 */
void interceptFinServeur(int sig) {

	printf("\rTerminaison du serveur\n");
	close(socketEcouteServeur);
	exit(EXIT_SUCCESS);
}

/**
 * Procédure de fermeture propre en cas d'erreur
 * @param msg
 */
void die(char* msg) {

	if (msg != NULL) printf("%s\n", msg);
	close(socketEcouteServeur);
	exit(EXIT_FAILURE);
}

/**
 * Procédure de fermeture propre en cas d'erreur
 * Même principe de die() mais nettoie addrServ
 * @param msg
 */
void dieAndFree(char* msg) {
	freeaddrinfo(addrServ);
	die(msg);
}

/**
 * Fonction exécutée par le thread pour gérer un client
 * @param socketClient File descriptor du socket client
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
int gestionClient(const int* socketClient) {

	int socketd = *socketClient; // On conserve une copie en cas de modification de la valeur par le père
	char buffer[LEN_BUFFER];
	ssize_t nbrBytesLus;

	while (1) {

		// Lecture du socket
		if ((nbrBytesLus = read(socketd, buffer, LEN_BUFFER)) == -1) {
			die("Erreur de lecture");
		}

		// Le socket a été fermé côté client donc on termine le thread
		if (nbrBytesLus == 0) {
			close(socketd);
			return EXIT_SUCCESS;
		}

		// On renvoie la chaine au client
		if (write(socketd, buffer, nbrBytesLus) == -1) {
			die("Erreur d'écriture");
		}
	}
}

/**
 * Programme principal en TCP (Mode connecté)
 */
int main(int argsNbr, char** args) {

	// On vérifie le nombre d'arguments et affiche une aide s'il en manque
	if (argsNbr < 2) {
		char* posSlash = strrchr(args[0], '/');
		printf("Usage: %s [port number]\n", posSlash == NULL ? args[0] : posSlash + 1);
		return EXIT_SUCCESS;
	}

	// 1 - 1023 Ports réservés
	// 1024 - 49151 Ports standards enregistrés
	// 49152 - 65535 Ports indéfinis
	long portNumber = strtol(args[1], NULL, 10);

	// On vérifie que le numéro de port est cohérent
	if (portNumber <= 0 || portNumber >= 65536) {
		printf("Le numéro de port doit être compris entre 1 et 65535\n");
		return EXIT_SUCCESS;
	} else if (portNumber < 1024 && getuid() != 0) {
		printf("Les numéros de ports inférieurs à 1024 sont réservés au super administrateur\n");
		return EXIT_SUCCESS;
	}

	// On passe à l'ouverture du socket
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET6; // V6 puisque ça va gérer le V4 ET v6 (encore une doc mal faite)
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	// On récupère les paramètres manquants
	int retour;
	if ((retour = getaddrinfo(NULL, args[1], &hints, &addrServ)) != 0) {
		printf("Error getaddrinfo: %s\n", gai_strerror(retour));
		dieAndFree(NULL);
	}

	// On ouvre le socket
	if ((socketEcouteServeur = socket(addrServ->ai_family, addrServ->ai_socktype, addrServ->ai_protocol)) == -1)
		dieAndFree("Erreur à l'ouverture du socket");

	// On rattache le socket au port
	if (bind(socketEcouteServeur, addrServ->ai_addr, addrServ->ai_addrlen) != 0)
		dieAndFree("Erreur de bind");

	// Enfin on écoute les connexions entrantes
	if (listen(socketEcouteServeur, 10) == -1)
		dieAndFree("Erreur de listen");

	freeaddrinfo(addrServ);

	// On intercepte les signaux pour une fermeture propre
	signal(SIGINT, interceptFinServeur);
	signal(SIGKILL, interceptFinServeur);
	signal(SIGTERM, interceptFinServeur);

	// On prépare les infos pour les connexions clients
	struct sockaddr_in6 addr_client;
	socklen_t len_addr_client = sizeof addr_client;
	pthread_t thread;
	int socketClient;

	#pragma clang diagnostic push
	#pragma ide diagnostic ignored "EndlessLoop"
	while (1) {

		// On attend une nouvelle demande de connexion
		if ((socketClient = accept(socketEcouteServeur, (struct sockaddr *) &addr_client, &len_addr_client)) == -1)
			die("Erreur à la connexion avec le client");

		// On affiche les détails du client
		char addrstr[INET6_ADDRSTRLEN];
		inet_ntop(addr_client.sin6_family, &addr_client.sin6_addr, addrstr, sizeof(addrstr));
		if (strncmp(addrstr, "::ffff:", 7) == 0) printf("Client connecté depuis %s Port: %d\n", &addrstr[7], ntohs(addr_client.sin6_port));
		else printf("Client connecté depuis %s Port: %d\n", addrstr, ntohs(addr_client.sin6_port));

		// On crée un thread pour gérer le client
		if (pthread_create(&thread, NULL, gestionClient, &socketClient) != 0)
			die("Erreur de création d'un thread");
	}
	#pragma clang diagnostic pop
}
