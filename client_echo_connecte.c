#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define LEN_BUFFER 16384

// File descriptor du socket
int socketd;
struct addrinfo* addrServ;

/**
 * Procédure d'interception de signaux pour une fermeture propre
 * @param sig
 */
void interceptFin(int sig) {

	printf("\rFin\n");
	close(socketd);
	exit(EXIT_SUCCESS);
}

/**
 * Programme principal en TCP (Mode connecté)
 */
int main(int argsNbr, char** args) {

	// On vérifie le nombre d'arguments et affiche une aide s'il en manque
	if (argsNbr < 3) {
		char* posSlash = strrchr(args[0], '/');
		printf("Usage: %s [host] [port number] (string to send)\n", posSlash == NULL ? args[0] : posSlash + 1);
		return EXIT_SUCCESS;
	}

	// 1 - 1023 Ports réservés
	// 1024 - 49151 Ports standards enregistrés
	// 49152 - 65535 Ports indéfinis
	long portNumber = strtol(args[2], NULL, 10);

	// On vérifie que le numéro de port est cohérent
	if (portNumber <= 0 || portNumber >= 65536) {
		printf("Le numéro de port doit être compris entre 1 et 65535\n");
		return EXIT_SUCCESS;
	} else if (portNumber < 1024 && getuid() != 0) {
		printf("Les numéros de ports inférieurs à 1024 sont réservés au super administrateur\n");
		return EXIT_SUCCESS;
	}

	// On prépare l'ouverture du socket
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // IPv4 ou v6
	hints.ai_socktype = SOCK_STREAM;
	((struct sockaddr_in *)&hints.ai_addr)->sin_port = htons(portNumber); // On spécifie le port du serveur

	// On récupère les paramètres manquants
	int retour;
	if ((retour = getaddrinfo(args[1], NULL, &hints, &addrServ)) != 0) {
		printf("Error getaddrinfo: %s\n", gai_strerror(retour));
		freeaddrinfo(addrServ);
		return EXIT_FAILURE;
	}

	((struct sockaddr_in *)addrServ->ai_addr)->sin_port = htons(portNumber); // On spécifie le port du serveur

	// Ouverture du socket
	if ((socketd = socket(addrServ->ai_family, addrServ->ai_socktype, addrServ->ai_protocol)) == -1) {
		printf("Erreur à l'ouverture du socket\n");
		freeaddrinfo(addrServ);
		return EXIT_FAILURE;
	}

	// On initie la session TCP
	if (connect(socketd, addrServ->ai_addr, addrServ->ai_addrlen) != 0) {
		printf("Impossible de se connecter au serveur\n");
		freeaddrinfo(addrServ);
		return EXIT_FAILURE;
	}

	freeaddrinfo(addrServ);

	// Interception des signaux pour une fin propre
	signal(SIGINT, interceptFin);
	signal(SIGKILL, interceptFin);
	signal(SIGTERM, interceptFin);

	// Buffer des messages
	char buffer[LEN_BUFFER];

	while(1) {

		if (argsNbr > 3) {

			// On initialise correctement le tableau et évite l'espace de début de chaine
			strncpy(buffer, args[3], LEN_BUFFER);
			for (int i = 4; i < argsNbr; ++i) {
				if (strlen(buffer) + 1 + strlen(args[i]) >= LEN_BUFFER) { // On évite les dépassements de tampon
					break;
				} else {
					strcat(strcat(buffer, " "), args[i]);
				}
			}
		} else {
			// Lecture sécurisée de stdin
			fgets(buffer, LEN_BUFFER, stdin);
		}

		// On renvoie la chaine au client
		if (write(socketd, buffer, strlen(buffer)) == -1) {
			printf("Erreur d'écriture\n");
			return EXIT_FAILURE;
		}

		// Lecture du socket
		switch (read(socketd, buffer, LEN_BUFFER)) {
			case -1:
				printf("Erreur de lecture\n");
				return EXIT_FAILURE;

			case 0: // Le socket a été fermé côté serveur donc on a terminé
				printf("Connexion fermée par le serveur\n");
				close(socketd);
				return EXIT_SUCCESS;

			default: // Affichage du message reçu.
				printf("%s\n", buffer);
				break;
		}

		// Chaine reçue en argument donc on sort
		if (argsNbr > 3) {
			interceptFin(SIGINT);
		}
	}
}

