# TP Sockets C

## Rendus

Les rendus sont disponibles dans la section release du repository

[Lien](https://gitlab.com/up6tech/S7-C-sockets/-/releases)

## Compiler avec CLion

Ouvrir le projet avec Clion

## Compiler avec Eclipse

Exécuter la commande suivante pour créer le fichier .projet nécessaire au fonctionnement d'Eclipse (compilation, debug, etc)<br>
``cmake -G "Eclipse CDT4 - Unix Makefiles" ./``

Pour d'autre IDE, veuillez remplacer `"Eclipse CDT4 - Unix Makefiles"` par la liste affichée via:<br>
``cmake -G``

Importer le projet via l'outil de l'IDE et choisir "Projet CMake"

## Via l'outil make des systèmes UNIX

Exécuter la commande suivante pour créer les fichiers make<br>
``cmake -G"Unix Makefiles" ./``
